import { items } from './items.js';

/**
 * Perform a search for items based on the provided query.
 *
 * @param {string} query - The search query
 * @return {Promise<Array<any>>} A promise resolving to the array of search results
 */
export async function searchItems(query) {
    return new Promise((resolve, reject) => {
        try {
            // Subscribe to the items store to get the current value
            const unsubscribe = items.subscribe($items => {
                const pattern = new RegExp(query, 'i');
                const searchResults = $items.flatMap(category =>
                    category.items.filter(item => pattern.test(item.label))
                );
                console.log(searchResults);
                resolve(searchResults);
                // Unsubscribe to prevent memory leaks
                unsubscribe();
            });
        } catch (error) {
            reject(error);
        }
    });
}


/**
 * Search for an exact item within a specific category.
 *
 * @param {string} category - The category to search within
 * @param {string} itemLabel - The label of the item to search for
 * @return {Promise<any>} A promise resolving to the found item, or null if not found
 */
export async function searchExactItem(category, itemLabel) {
    return new Promise((resolve, reject) => {
        try {
            // Subscribe to the items store to get the current value
            const unsubscribe = items.subscribe($items => {
                // Find the category by label
                const foundCategory = $items.find(cat => cat.label === category);
                if (foundCategory) {
                    // Find the exact item within the category
                    const foundItem = foundCategory.items.find(item => item.label === itemLabel);

                    resolve(foundItem || null); // Resolve with the found item or null if not found
                } else {
                    resolve(null); // Resolve with null if the category is not found
                }
                // Unsubscribe to prevent memory leaks
                unsubscribe();
            });
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Search for a category by its label.
 *
 * @param {string} categoryLabel - The label of the category to search for
 * @return {Promise<any>} A promise resolving to the found category object, or null if not found
 */
export async function searchCategory(categoryLabel) {
    return new Promise((resolve, reject) => {
        try {
            // Subscribe to the items store to get the current value
            const unsubscribe = items.subscribe($items => {
                // Find the category by label
                const foundCategory = $items.find(cat => cat.label === categoryLabel);
                resolve(foundCategory || null); // Resolve with the found category or null if not found
                // Unsubscribe to prevent memory leaks
                unsubscribe();
            });
        } catch (error) {
            reject(error);
        }
    });
}
