import { writable } from 'svelte/store';
import data from './transformed_products.json';

export const items = writable(data); 