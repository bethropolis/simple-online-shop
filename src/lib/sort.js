import fs from 'fs';

// Read the existing JSON file
fs.readFile('src/lib/products.json', 'utf8', (err, data) => {
    if (err) {
        console.error("Error reading file:", err);
        return;
    }

    try {
        // Parse the JSON data
        const products = JSON.parse(data);

        // Transform the data into the desired format
        const transformedData = Object.keys(products).map(category => {
            return {
                label: category,
                items: products[category].map(item => {item.category = category; return item;})
            };
        });

        // Write the transformed data to a new file
        fs.writeFile('src/lib/transformed_products.json', JSON.stringify(transformedData, null, 2), err => {
            if (err) {
                console.error("Error writing file:", err);
                return;
            }
            console.log("File has been written successfully!");
        });
    } catch (error) {
        console.error("Error parsing JSON:", error);
    }
});
